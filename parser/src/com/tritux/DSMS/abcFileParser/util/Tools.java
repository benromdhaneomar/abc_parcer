package com.tritux.DSMS.abcFileParser.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Tools {
	private final static Logger LOGGER = Logger.getLogger(Tools.class.getName());
	private String fileName;
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Tools(){
	}
	public String getPropertyValue(String property) {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream(this.getFileName());
			prop.load(input);
			return prop.getProperty(property);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	public void initLog() {
		LOGGER.setLevel(Level.ALL);
		ConsoleHandler ch=new ConsoleHandler();
		LOGGER.addHandler(ch);
		FileHandler fh;
		try {
			fh = new FileHandler(this.getPropertyValue("abc.log.file.location")+System.getProperty("file.separator")+this.getPropertyValue("abc.log.file.name"),true);
			LOGGER.addHandler(fh);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		LOGGER.setLevel(Level.parse(this.getPropertyValue("abc.log.file.level")));
	}
	public void logging(String msg, Level level) {
		if(null==level||level==Level.INFO)
			LOGGER.info(msg);
		if(level==Level.CONFIG)
			LOGGER.config(msg);
		if(level==Level.FINE)
			LOGGER.fine(msg);
		if(level==Level.FINER)
			LOGGER.finer(msg);
		if(level==Level.FINEST)
			LOGGER.finest(msg);
		if(level==Level.SEVERE)
			LOGGER.severe(msg);
		if(level==Level.WARNING)
			LOGGER.warning(msg);
	}
	public String getTodaysDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		return dateFormat.format(date);
	}
}
