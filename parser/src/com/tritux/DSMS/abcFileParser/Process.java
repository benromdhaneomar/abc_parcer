package com.tritux.DSMS.abcFileParser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.logging.Level;

import org.springframework.beans.factory.annotation.Autowired;

import com.tritux.DSMS.abcFileParser.exception.AbcSourceFileIsNotAccessibleException;
import com.tritux.DSMS.abcFileParser.exception.AddressbookCreationAccessException;
import com.tritux.DSMS.abcFileParser.exception.CmpCreationAccessException;
import com.tritux.DSMS.abcFileParser.exception.SourceDirectoryIsNotAccessibleException;
import com.tritux.DSMS.abcFileParser.exception.TemporaryAddressBookFileAccessException;
import com.tritux.DSMS.abcFileParser.exception.TemporaryCompanyFileAccessException;
import com.tritux.DSMS.abcFileParser.exception.TemporaryDirectoryIsNotAccessibleException;
import com.tritux.DSMS.abcFileParser.exception.TemporaryFileProblemException;
import com.tritux.DSMS.abcFileParser.util.Tools;

public class Process implements Runnable {
	
	@Autowired
	private Tools tools;

	public Tools getTools() {
		return tools;
	}

	public void setTools(Tools tools) {
		this.tools = tools;
	}

	public void run() {
		this.processing();
	}
	
	private void processing() {
		File SourceDirectory=new File(tools.getPropertyValue("abc.source.file.location"));
		try {
			tools.logging("get ABC files list", null);
			File[] sorceFileList = this.getSourceDirectoryFilesList(SourceDirectory);
			for (File ABCFile : sorceFileList) {
				tools.logging("treating file "+ABCFile.getAbsolutePath(), null);
				File temporaryFolder=new File(tools.getPropertyValue("abc.temporary.folder"));
				File temporaryAbcFile = this.copyABCSourceFileToTempFolder(ABCFile, temporaryFolder);
				tools.logging("create temporary ABC file"+temporaryAbcFile.getAbsolutePath(), null);
				String Id = this.generateCmpABID(Integer.valueOf(tools.getPropertyValue("abc.destination.file.id.length")));
				tools.logging("generating id "+Id, null);
				File tempCmpFile = this.createTempCompanyFile(Id, temporaryFolder, tools.getPropertyValue("abc.destination.file.campaign.prefix"), 
						tools.getPropertyValue("abc.msg.source"), tools.getTodaysDate(), tools.getPropertyValue("abc.cmp.time.start"), 
						tools.getPropertyValue("abc.cmp.time.stop"), tools.getPropertyValue("abc.destination.file.addressbook.prefix"));
				tools.logging("create temporary cmp file"+tempCmpFile.getAbsolutePath(), null);
				File tempAddressBookFile = this.createTempAddressBookFile(Id, temporaryFolder, tools.getPropertyValue("abc.destination.file.addressbook.prefix"), temporaryAbcFile);
				tools.logging("create temporary address book file "+tempAddressBookFile, null);
				File DsmsWorkingFolder=new File(tools.getPropertyValue("abc.destination.file.location"));
				File addressBookFile = this.copyTempAdressBookFileToDSMSFolder(tempAddressBookFile, DsmsWorkingFolder);
				tools.logging("creating address book file into working directory "+addressBookFile, null);
				File cmpFile = this.copyTempcompFileToDSMSFolder(tempCmpFile, DsmsWorkingFolder);
				tools.logging("creating comp file into working directory "+cmpFile, null);
			}
		} catch (SourceDirectoryIsNotAccessibleException e) {
			tools.logging("SourceDirectoryIsNotAccessibleException"+e.getMessage(), Level.SEVERE);
		} catch (AbcSourceFileIsNotAccessibleException e) {
			tools.logging("AbcSourceFileIsNotAccessibleException"+e.getMessage(), Level.SEVERE);
		} catch (TemporaryDirectoryIsNotAccessibleException e) {
			tools.logging("TemporaryDirectoryIsNotAccessibleException"+e.getMessage(), Level.SEVERE);
		} catch (TemporaryFileProblemException e) {
			tools.logging("TemporaryFileProblemException"+e.getMessage(), Level.SEVERE);
		} catch (TemporaryCompanyFileAccessException e) {
			tools.logging("TemporaryCompanyFileAccessException"+e.getMessage(), Level.SEVERE);
		} catch (TemporaryAddressBookFileAccessException e) {
			tools.logging("TemporaryAddressBookFileAccessException"+e.getMessage(), Level.SEVERE);
		} catch (AddressbookCreationAccessException e) {
			tools.logging("AddressbookCreationAccessException"+e.getMessage(), Level.SEVERE);
		} catch (CmpCreationAccessException e) {
			tools.logging("CmpCreationAccessException"+e.getMessage(), Level.SEVERE);
		}
	}
	
	private File[] getSourceDirectoryFilesList(File SourceDirectory) throws SourceDirectoryIsNotAccessibleException {
		if(SourceDirectory.exists()&&SourceDirectory.isDirectory()) {
			return SourceDirectory.listFiles();
		}
		else {
			throw new SourceDirectoryIsNotAccessibleException();
		}
	}
	
	private File copyABCSourceFileToTempFolder(File abcSourceFile, File tempDirectory) throws AbcSourceFileIsNotAccessibleException, TemporaryDirectoryIsNotAccessibleException, TemporaryFileProblemException {
		if(abcSourceFile.exists()&&abcSourceFile.isFile()&&abcSourceFile.canRead()) {
			if(tempDirectory.exists()) {
				if(tempDirectory.isDirectory()) {
					File tmpAbcFile=new File(tempDirectory.getAbsolutePath()+System.getProperty("file.separator")+abcSourceFile.getName()+"_tmp.txt");
					if(!abcSourceFile.renameTo(tmpAbcFile)) {
						throw new TemporaryFileProblemException();
					}
					else {
						return tmpAbcFile;
					}
				}
				else {
					throw new TemporaryDirectoryIsNotAccessibleException();
				}
			}
			else {
				if(tempDirectory.mkdir()) {
					File tmpAbcFile=new File(tempDirectory.getAbsolutePath()+System.getProperty("file.separator")+abcSourceFile.getName()+"_tmp.txt");
					if(abcSourceFile.renameTo(tmpAbcFile)) {
						throw new TemporaryFileProblemException();
					}
					else {
						return tmpAbcFile;
					}
				}
				else {
					throw new TemporaryDirectoryIsNotAccessibleException();
				}
			}
		}
		else {
			throw new AbcSourceFileIsNotAccessibleException();
		}
	}
	
	private String generateCmpABID(int length) {
		return String.valueOf(String.valueOf(System.currentTimeMillis()).hashCode()).substring(0, length-1).substring(1);
	}
	
	private File createTempCompanyFile(String id, File tempDirectory, String cmpPrefix, String msgSource, String todaysDate,
			String startHour, String stopHour, String addressBookPrefix) throws TemporaryCompanyFileAccessException {
		File tmpCmpFile=new File(tempDirectory.getAbsolutePath()+System.getProperty("file.separator")+cmpPrefix+id+"_tmp.txt");
		if(tmpCmpFile.exists()) {
			if(!tmpCmpFile.delete()){
				throw new TemporaryCompanyFileAccessException();
			}
		}
		else {
			try{
				if(!tmpCmpFile.createNewFile()) {
					throw new TemporaryCompanyFileAccessException();
				}
			}
			catch(IOException e) {
				throw new TemporaryCompanyFileAccessException();
			}
		}
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(tmpCmpFile));
			writer.write("--campaign\n");
			System.out.println("--campaign\n");
			writer.write("campaign_id:"+cmpPrefix+id+"\n");
			System.out.println("campaign_id:"+cmpPrefix+id+"\n");
			writer.write("from:"+msgSource+"\n");
			System.out.println("from:"+msgSource+"\n");
			writer.write("start_at:"+todaysDate+"\n");
			System.out.println("start_at:"+todaysDate+"\n");
			writer.write("end_at:"+todaysDate+"\n");
			System.out.println("end_at:"+todaysDate+"\n");
			writer.write("start_hour:"+startHour+"\n");
			System.out.println("start_hour:"+startHour+"\n");
			writer.write("stop_hour:"+stopHour+"\n");
			System.out.println("stop_hour:"+stopHour+"\n");
			writer.write("sms_content:$sms\n");
			System.out.println("sms_content:$sms\n");
			writer.write("use_addressbook:"+addressBookPrefix+id+"\n");
			System.out.println("use_addressbook:"+addressBookPrefix+id+"\n");
			writer.write("-- BEGIN QUEUE");
			System.out.println("-- BEGIN QUEUE");
			writer.close();
		}
		catch(IOException e) {
			throw new TemporaryCompanyFileAccessException();
		}
		return tmpCmpFile;
	}
	
	private File createTempAddressBookFile(String id, File tempDirectory, String addressBookPrefix, File abcTempFile) throws TemporaryAddressBookFileAccessException {
		File tmpAddressBookFile=new File(tempDirectory.getAbsolutePath()+System.getProperty("file.separator")+addressBookPrefix+id+"_tmp.txt");
		if(tmpAddressBookFile.exists()) {
			if(!tmpAddressBookFile.delete()){
				throw new TemporaryAddressBookFileAccessException();
			}
		}
		else {
			try{
				if(!tmpAddressBookFile.createNewFile()) {
					throw new TemporaryAddressBookFileAccessException();
				}
			}
			catch(IOException e) {
				throw new TemporaryAddressBookFileAccessException();
			}
		}
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(tmpAddressBookFile));
			writer.write("--addressbook\n");
			System.out.println("--addressbook\n");
			writer.write("addressbook_id:"+addressBookPrefix+id+"\n");
			System.out.println("addressbook_id:"+addressBookPrefix+id+"\n");
			writer.write("macros:sms\n");
			System.out.println("macros:sms\n");
			writer.write(tools.getPropertyValue("abc.notify.desteny")+";initab\n");
			System.out.println(tools.getPropertyValue("abc.notify.desteny")+";initab\n");
			FileReader fileReader = new FileReader(abcTempFile);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line;
			
			while ((line = bufferedReader.readLine()) != null) {
				StringTokenizer st = new StringTokenizer(line,"|");
				int tmpToken = st.countTokens();
				String lineParts0=st.nextToken();
				lineParts0=lineParts0.replaceAll("\\s+","");
				String lineParts1=st.nextToken();
				if(tmpToken==2) {
					writer.write("216"+lineParts0+";"+lineParts1+"\n");
					System.out.println("216"+lineParts0+";"+lineParts1+"\n");
				}
				else {
					tools.logging("ignored message",Level.WARNING);
					continue;
				}
			}
			bufferedReader.close();
			writer.close();
		}
		catch(IOException e) {
			throw new TemporaryAddressBookFileAccessException();
		}
		return tmpAddressBookFile;
	}
	
	private File copyTempAdressBookFileToDSMSFolder(File temporaryAddressBookFile, File dsmsFolder) throws AddressbookCreationAccessException {
		if(dsmsFolder.exists()&&dsmsFolder.isDirectory()) {
			File AddressBookFile=new File(dsmsFolder.getAbsolutePath()+System.getProperty("file.separator")+temporaryAddressBookFile.getName().substring(0, temporaryAddressBookFile.getName().length()-8)+".txt");
			if(temporaryAddressBookFile.renameTo(AddressBookFile)) {
				return AddressBookFile;
			}
			else {
				throw new AddressbookCreationAccessException();
			}
		}
		else {
			throw new AddressbookCreationAccessException();
		}
	}
	
	private File copyTempcompFileToDSMSFolder(File temporarycmpFile, File dsmsFolder) throws AddressbookCreationAccessException, CmpCreationAccessException {
		if(dsmsFolder.exists()&&dsmsFolder.isDirectory()) {
			File cmpFile=new File(dsmsFolder.getAbsolutePath()+System.getProperty("file.separator")+temporarycmpFile.getName().substring(0, temporarycmpFile.getName().length()-8)+".txt");
			if(temporarycmpFile.renameTo(cmpFile)) {
				return cmpFile;
			}
			else {
				throw new CmpCreationAccessException();
			}
		}
		else {
			throw new CmpCreationAccessException();
		}
	}
}
