package com.tritux.DSMS.abcFileParser;


import java.util.logging.Level;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.tritux.DSMS.abcFileParser.util.Tools;

public class Main {
	
	
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("abcFileParser_spring_main.xml");
		Tools tools=(Tools) context.getBean("tools");
		tools.initLog();
		tools.logging("***start parser***", null);
		while(true) {
			tools.logging("**boucle**", null);
			Process process=(Process) context.getBean("process");;
			Thread thread=new Thread(process);
			tools.logging("*sleep*", null);
			thread.start();
			try {
				Thread.sleep(Long.valueOf(tools.getPropertyValue("abc.thread.timetowait")));
			} catch (InterruptedException e) {
				tools.logging("error threading"+e.getMessage(), Level.SEVERE);
			}
		}
		//((ConfigurableApplicationContext)context).close();
	}
}
